﻿/*
 *   Copyright (c) 2020 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    private PlayerController player;
    
    // Start is called before the first frame update
    void Start()
    {
        GetPlayerComponent();
    }

    // Update is called once per frame
    void Update()
    {
        CamFollowPlayer();
    }
    
    private void GetPlayerComponent()
    {
        player = FindObjectOfType<PlayerController>();
    }

    private void CamFollowPlayer()
    {
        Vector2 newCamPos = new Vector2(player.transform.position.x,player.transform.position.y);
        transform.position = new Vector3(newCamPos.x,newCamPos.y,transform.position.z);
    }
}
