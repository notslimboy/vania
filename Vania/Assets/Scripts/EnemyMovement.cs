﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    [SerializeField] private float moveSpeed = 10f;
    private Rigidbody2D rigidBody2D;

    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    
    private void Movement()
    {
        rigidBody2D.velocity = new Vector2(moveSpeed,0f);
        if (!IsFacingRight())
        {
            rigidBody2D.velocity = new Vector2(-moveSpeed,0f);    
        }
    }
    
    private bool IsFacingRight()
    {
        return transform.localScale.x > 0;
    }

    private void GetComponent()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        transform.localScale = new Vector2(-(Mathf.Sign(rigidBody2D.velocity.x)), 1f);
    }
}
