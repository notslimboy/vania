﻿/*
 *   Copyright (c) 2020 NotSlimBoy
 *   All rights reserved.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    // * Config
    [SerializeField] private float runSpeed = 10f;
    [SerializeField] private float jumpPower = 10f;
    [SerializeField] private float climbSpeed = 10f;
    
    
    // * Component References 
    Rigidbody2D rigidBody2D;
    Animator animator;
    Collider2D collider;
    private float gravityScaleAtStart;
    
    // * Player state
    private bool isAlive = true;


    private void Start()
    {
        GetComponent();
    }

    private void GetComponent()
    {
        animator = GetComponent<Animator>();
        rigidBody2D = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        gravityScaleAtStart = rigidBody2D.gravityScale;
    }

    private void Update()
    {
        PlayerControl();
    }

    private void PlayerControl()
    {
        // * Behaviour
        PlayerBehaviour();
        
        // * Animation
        PlayerAnimationController();
    }

    private void PlayerBehaviour()
    {
        Run();
        Jump();
        ClimbLadder();
    }

    private void PlayerAnimationController()
    {
        FlipSprite();
        PlayerRunAnimation();
        PlayerClimbingAnimation();
    }

    private void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal"); // * value is betweeen -1 to +1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, rigidBody2D.velocity.y);
        rigidBody2D.velocity = playerVelocity;
    }

    private void Jump()
    {
        if (collider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            if (CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                Vector2 jumpVelocity = new Vector2(0f,jumpPower);
                rigidBody2D.velocity += jumpVelocity;
            }
        }
    }
    
    private void ClimbLadder()
    {
        if (collider.IsTouchingLayers(LayerMask.GetMask("Ladder")))
        {
            float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
            Vector2 climbingVelocity = new Vector2(rigidBody2D.velocity.x,controlThrow * climbSpeed);
            rigidBody2D.velocity = climbingVelocity;
            rigidBody2D.gravityScale = 0f; // * set Gravity scale to zero if hit the ladder layer
        }
        else
        {
            rigidBody2D.gravityScale = gravityScaleAtStart;
            animator.SetBool("Climbing", false);
        }
    }
    
    private void PlayerClimbingAnimation()
    {
        bool playerHasVerticalSpeed = Mathf.Abs(rigidBody2D.velocity.y) > Mathf.Epsilon;
        animator.SetBool("Climbing", playerHasVerticalSpeed);
    }

    private void PlayerRunAnimation()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(rigidBody2D.velocity.x) > Mathf.Epsilon;
        animator.SetBool("Running", playerHasHorizontalSpeed);
    }
    
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(rigidBody2D.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(rigidBody2D.velocity.x), 1f);
        }
    }
    
   
    
    
}
